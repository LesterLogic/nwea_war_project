'use strict';

// Define the gameTable component
angular.module('gameTable').component('gameTable', {
    templateUrl: 'app/game-table/game-table.template.html',
    controller: ['$scope', 'deckFactory', 'playerFactory', function GameTableController($scope, deck, playerFactory) {
        var self = this;
        this.state = 'selecting';
        this.deck = [];
        this.players = [];
        this.winner = 0;

        //Get a new deck, new players, and set the state to playing
        this.startGame = function(numberOfPlayers) {
            self.getNewDeck();
            self.loadPlayers(numberOfPlayers);
            self.state = 'playing';
        }

        //Get a new deck of cards from the deckFactory and shuffle it
        this.getNewDeck = function() {
            self.deck = deck.getCards();
            self.deck = deck.shuffle(self.deck);
        }

        //Get new players from the playerFactory and deal their cards
        this.loadPlayers = function(numberOfPlayers) {
            for (var x=1; x<=numberOfPlayers; x++) {
                var divisor = self.deck.length/numberOfPlayers;
                var start = divisor * (x-1);
                var end = divisor * x;
                var cards = self.deck.slice(start, end);
                self.players.push(playerFactory.getNewPlayer(cards));
            }
        }

        //If all active players have played, check card values for a possible state change
        this.evaluateCards = function() {
            if (self.checkRoundComplete()) {
                var winningValues = [];
                for (var x=0; x<self.players.length; x++) {
                    if (self.players[x].isAlive()) {
                        winningValues.push(self.players[x].getActiveCardValue());
                    }
                }
                winningValues = self.sortActiveCardValues(winningValues);
                if (winningValues[0] === winningValues[1]) {
                    self.state = 'war';
                    //Pause here to give the player a chance to see what's happening.
                    //This breaks out of the digest cycle so we have to call $apply to update the view
                    setTimeout(function() { self.continuePlay(0); $scope.$apply(); }, 600, self, $scope);
                } else {
                    var winningValue = winningValues[0];
                    self.state = 'playing';
                    for (var x=0; x<self.players.length; x++) {
                        if (self.players[x].getActiveCardValue() === winningValue) {
                            var winner = self.players[x];
                            winner.setState('winner');
                            break;
                        }
                    }
                    //Pause here to give the player a chance to see what's happening.
                    //This breaks out of the digest cycle so we have to call $apply to update the view
                    setTimeout(function() { self.continuePlay(winningValue); $scope.$apply(); }, 3000, self, winningValue, $scope);
                }
            }
        }

        //Check the state of the game and continue with processing the turn
        this.continuePlay = function(winningValue) {
            if (self.state === 'war') {
                //Move player cards into the hopper and clear the active cards
                for (var x=0; x<self.players.length; x++) {
                    self.players[x].moveActiveCardsToHopper();
                }
            } else if (self.state === 'playing') {
                var winner = null;
                var spoils = [];
                //Figure out which player won the round, collect all the active and hopper cards
                for (var x=0; x<self.players.length; x++) {
                    if (self.players[x].getActiveCardValue() === winningValue) {
                        winner = self.players[x];
                    }
                    spoils = spoils.concat(self.players[x].forfeitCards());
                    spoils = deck.shuffle(spoils);
                }
                self.winningValue = 0;
                //There should always be a winner, but just in case...
                //Give all the collected cards to the winner and set the player state back to default
                if (winner !== null) {
                    winner.acceptCards(spoils);
                    winner.setState('default');
                }
                //All actions for the round are complete, check if there is a winner
                self.checkForWinState();
            }
        }

        //Sort values in descending value to make the victory check ever so slightly faster
        this.sortActiveCardValues = function(values) {
            return values.sort(function(a, b){
                if (a > b) {
                    return -1;
                }
                if (a < b) {
                    return 1;
                }
                return 0;
            });
        }

        //Check the active property on all players to see if everyone has played
        this.checkRoundComplete = function() {
            var roundComplete = true;
            for (var x=0; x<self.players.length; x++) {
                if (self.players[x].isAlive()) {
                    if (self.players[x].getActiveCardValue() === false) {
                        roundComplete = false;
                    }
                }
            }

            return roundComplete;
        }

        //Check if one player has won all the cards
        this.checkForWinState = function() {
            var playersWithCards = [];
            //Put the index of all players with cards into a list...
            for (var x=0; x<self.players.length; x++) {
                if (self.players[x].stack.length > 0) {
                    playersWithCards.push(x);
                }
                self.players[x].verifyAliveStatus();
            }
            //Check if that list has only one index, that's our winner
            if (playersWithCards.length === 1) {
                self.showWinScreen(playersWithCards[0]);
            }
        }

        //Switch the game state to show the winning state view
        this.showWinScreen = function(playerNumber) {
            this.state = 'complete';
            this.winner = playerNumber;
        }

        //Set the game back to default values in preparation for a new game
        this.resetGame = function() {
            self.state = 'selecting';
            self.deck = [];
            self.players = [];
            self.winner = 0;
        }
    }]
});