'use strict';

// Define the gameTable module
// This module contains the core game logic and state keeping
angular.module('gameTable', ['ngRoute', 'deck', 'player']);