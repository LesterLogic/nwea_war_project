describe('component: gameTable', function() {
    var $componentController;

    beforeEach(module('warApp'));
    beforeEach(inject(function(_$componentController_) {
        $componentController = _$componentController_;
        ctrl = $componentController('gameTable');
    }));

    it('should load with default settings', function() {
        expect(ctrl.state).toBe('selecting');
        expect(ctrl.deck.length).toEqual(0);
        expect(ctrl.players.length).toEqual(0);
    });

    it('should load starting values', function() {
        ctrl.startGame(2);

        expect(ctrl.state).toBe('playing');
        expect(ctrl.players.length).toEqual(2);
        expect(ctrl.deck.length).toEqual(52);
    });

    it('should load new deck', function() {
        ctrl.getNewDeck();
        expect(ctrl.deck.length).toEqual(52);
    });

    it('should load specified number of players with correct number of cards', function() {
        ctrl.deck = [{ suit: 'clubs', value: 3, image: './app/deck/res/3c.png', imageBack: './app/deck/res/back.png' },
                 { suit: 'diamonds', value: 11, image: './app/deck/res/jd.png', imageBack: './app/deck/res/back.png' },
                 { suit: 'diamonds', value: 5, image: './app/deck/res/5d.png', imageBack: './app/deck/res/back.png' },
                 { suit: 'clubs', value: 13, image: './app/deck/res/kc.png', imageBack: './app/deck/res/back.png' }];
        ctrl.loadPlayers(2);

        expect(ctrl.players.length).toEqual(2);
        expect(ctrl.players[0].stack.length).toEqual(2);
        expect(ctrl.players[1].stack.length).toEqual(2);
    });

    it('should return false if there is a player with no active card', function() {
        ctrl.deck = [{ suit: 'clubs', value: 3, image: './app/deck/res/3c.png', imageBack: './app/deck/res/back.png' },
                 { suit: 'diamonds', value: 11, image: './app/deck/res/jd.png', imageBack: './app/deck/res/back.png' },
                 { suit: 'diamonds', value: 5, image: './app/deck/res/5d.png', imageBack: './app/deck/res/back.png' },
                 { suit: 'clubs', value: 13, image: './app/deck/res/kc.png', imageBack: './app/deck/res/back.png' }];
        ctrl.loadPlayers(2);

        ctrl.players[0].active = [{ suit: 'clubs', value: 13, image: './app/deck/res/kc.png', imageBack: './app/deck/res/back.png' }];
        complete = ctrl.checkRoundComplete();
        expect(complete).toBe(false);
    });

    it('should return true if both players have active cards', function() {
        ctrl.deck = [{ suit: 'clubs', value: 3, image: './app/deck/res/3c.png', imageBack: './app/deck/res/back.png' },
                 { suit: 'diamonds', value: 11, image: './app/deck/res/jd.png', imageBack: './app/deck/res/back.png' },
                 { suit: 'diamonds', value: 5, image: './app/deck/res/5d.png', imageBack: './app/deck/res/back.png' },
                 { suit: 'clubs', value: 13, image: './app/deck/res/kc.png', imageBack: './app/deck/res/back.png' }];
        ctrl.loadPlayers(2);
        ctrl.players[0].active = [{ suit: 'clubs', value: 13, image: './app/deck/res/kc.png', imageBack: './app/deck/res/back.png' }];
        ctrl.players[1].active = [{ suit: 'clubs', value: 13, image: './app/deck/res/kc.png', imageBack: './app/deck/res/back.png' }];

        complete = ctrl.checkRoundComplete();
        expect(complete).toBe(true);

    });

    it('should sort array of numbers in descending value', function() {
        values = [2, 14, 6, 9];
        sorted = ctrl.sortActiveCardValues(values);
        expect(sorted).toEqual([14, 9, 6, 2]);
    });

    it('should evaluate active cards and set game state to war', function() {
        ctrl.deck = [{ suit: 'clubs', value: 3, image: './app/deck/res/3c.png', imageBack: './app/deck/res/back.png' },
                 { suit: 'diamonds', value: 11, image: './app/deck/res/jd.png', imageBack: './app/deck/res/back.png' },
                 { suit: 'diamonds', value: 5, image: './app/deck/res/5d.png', imageBack: './app/deck/res/back.png' },
                 { suit: 'clubs', value: 13, image: './app/deck/res/kc.png', imageBack: './app/deck/res/back.png' }];
        ctrl.loadPlayers(2);
        ctrl.players[0].active = [{ suit: 'spades', value: 6, image: './app/deck/res/6s.png', imageBack: './app/deck/res/back.png' }];
        ctrl.players[1].active = [{ suit: 'hearts', value: 6, image: './app/deck/res/6c.png', imageBack: './app/deck/res/back.png' }];

        ctrl.evaluateCards();
        expect(ctrl.state).toBe('war');
    });

    it('should evaluate active cards and adjust game state to playing', function() {
        ctrl.deck = [{ suit: 'clubs', value: 3, image: './app/deck/res/3c.png', imageBack: './app/deck/res/back.png' },
                 { suit: 'diamonds', value: 11, image: './app/deck/res/jd.png', imageBack: './app/deck/res/back.png' },
                 { suit: 'diamonds', value: 5, image: './app/deck/res/5d.png', imageBack: './app/deck/res/back.png' },
                 { suit: 'clubs', value: 13, image: './app/deck/res/kc.png', imageBack: './app/deck/res/back.png' }];
        ctrl.loadPlayers(2);
        ctrl.players[0].active = [{ suit: 'spades', value: 6, image: './app/deck/res/6s.png', imageBack: './app/deck/res/back.png' }];
        ctrl.players[1].active = [{ suit: 'hearts', value: 12, image: './app/deck/res/qc.png', imageBack: './app/deck/res/back.png' }];

        ctrl.evaluateCards();
        expect(ctrl.state).toBe('playing');
    });

    it('should move active cards into player hopper based on game state', function() {
        ctrl.deck = [{ suit: 'clubs', value: 3, image: './app/deck/res/3c.png', imageBack: './app/deck/res/back.png' },
                 { suit: 'diamonds', value: 11, image: './app/deck/res/jd.png', imageBack: './app/deck/res/back.png' },
                 { suit: 'diamonds', value: 5, image: './app/deck/res/5d.png', imageBack: './app/deck/res/back.png' },
                 { suit: 'clubs', value: 13, image: './app/deck/res/kc.png', imageBack: './app/deck/res/back.png' }];
        ctrl.loadPlayers(2);
        ctrl.players[0].active = [{ suit: 'spades', value: 6, image: './app/deck/res/6s.png', imageBack: './app/deck/res/back.png' }];
        ctrl.players[1].active = [{ suit: 'hearts', value: 12, image: './app/deck/res/qc.png', imageBack: './app/deck/res/back.png' }];
        ctrl.state = 'war';
        var winningValue = 12;

        ctrl.continuePlay(winningValue);

        expect(ctrl.players[0].active.length).toEqual(0);
        expect(ctrl.players[0].hopper.length).toEqual(1);
        expect(ctrl.players[1].active.length).toEqual(0);
        expect(ctrl.players[1].hopper.length).toEqual(1);
    });

    it('should active cards into winning player stack based on game state', function() {
        ctrl.deck = [{ suit: 'clubs', value: 3, image: './app/deck/res/3c.png', imageBack: './app/deck/res/back.png' },
                 { suit: 'diamonds', value: 11, image: './app/deck/res/jd.png', imageBack: './app/deck/res/back.png' },
                 { suit: 'diamonds', value: 5, image: './app/deck/res/5d.png', imageBack: './app/deck/res/back.png' },
                 { suit: 'clubs', value: 13, image: './app/deck/res/kc.png', imageBack: './app/deck/res/back.png' }];
        ctrl.loadPlayers(2);
        ctrl.players[0].active = [{ suit: 'spades', value: 6, image: './app/deck/res/6s.png', imageBack: './app/deck/res/back.png' }];
        ctrl.players[1].active = [{ suit: 'hearts', value: 12, image: './app/deck/res/qc.png', imageBack: './app/deck/res/back.png' }];
        ctrl.state = 'playing';
        var winningValue = 12;

        ctrl.continuePlay(winningValue);

        expect(ctrl.players[0].active.length).toEqual(0);
        expect(ctrl.players[0].stack.length).toEqual(2);
        expect(ctrl.players[1].active.length).toEqual(0);
        expect(ctrl.players[1].stack.length).toEqual(4);
    });

    it('should check all players to determine if only on player has cards left', function() {
        ctrl.deck = [{ suit: 'clubs', value: 3, image: './app/deck/res/3c.png', imageBack: './app/deck/res/back.png' },
                 { suit: 'diamonds', value: 11, image: './app/deck/res/jd.png', imageBack: './app/deck/res/back.png' },
                 { suit: 'diamonds', value: 5, image: './app/deck/res/5d.png', imageBack: './app/deck/res/back.png' },
                 { suit: 'clubs', value: 13, image: './app/deck/res/kc.png', imageBack: './app/deck/res/back.png' }];
        ctrl.loadPlayers(2);
        ctrl.players[1].stack = [];

        ctrl.checkForWinState();

        expect(ctrl.state).toBe('complete');
        expect(ctrl.winner).toEqual(0);
    });

    it('should set game state to complete and set winning player index', function() {
        ctrl.showWinScreen(3);

        expect(ctrl.state).toBe('complete');
        expect(ctrl.winner).toEqual(3);
    });

    it('should reset game to default settings', function() {
        ctrl.deck = [{ suit: 'clubs', value: 3, image: './app/deck/res/3c.png', imageBack: './app/deck/res/back.png' },
         { suit: 'diamonds', value: 11, image: './app/deck/res/jd.png', imageBack: './app/deck/res/back.png' },
         { suit: 'diamonds', value: 5, image: './app/deck/res/5d.png', imageBack: './app/deck/res/back.png' },
         { suit: 'clubs', value: 13, image: './app/deck/res/kc.png', imageBack: './app/deck/res/back.png' }];
        ctrl.loadPlayers(2);
        ctrl.state = 'playing';
        ctrl.winner = 2;

        ctrl.resetGame();

        expect(ctrl.deck.length).toEqual(0);
        expect(ctrl.players.length).toEqual(0);
        expect(ctrl.state).toBe('selecting');
        expect(ctrl.winner).toEqual(0);
    });
});