describe('component: gameTable', function() {
    var $componentController;

    beforeEach(module('warApp'));
    beforeEach(inject(function(_$componentController_, _playerFactory_) {
        $componentController = _$componentController_;
        playerFactory = _playerFactory_;
        gameTable = $componentController('gameTable');
        cards =[{ suit: 'clubs', value: 3, image: './app/deck/res/3c.png', imageBack: './app/deck/res/back.png' },
                 { suit: 'diamonds', value: 11, image: './app/deck/res/jd.png', imageBack: './app/deck/res/back.png' },
                 { suit: 'diamonds', value: 5, image: './app/deck/res/5d.png', imageBack: './app/deck/res/back.png' },
                 { suit: 'clubs', value: 13, image: './app/deck/res/kc.png', imageBack: './app/deck/res/back.png' }];
        newPlayer = playerFactory.getNewPlayer(cards);
        var bindings = {player: newPlayer, index: 1, gameTable: gameTable};
        ctrl = $componentController('player', null, bindings);
    }));

    it('should check if there is an active card and draw 1 card', function() {
        ctrl.gameTable.state = 'playing';
        ctrl.playTurn();
        expect(ctrl.player.active.length).toEqual(1);
    });

    it('should check if there is an active card and draw 2 cards', function() {
        ctrl.gameTable.state = 'war';
        ctrl.playTurn();
        expect(ctrl.player.active.length).toEqual(2);
    });
});