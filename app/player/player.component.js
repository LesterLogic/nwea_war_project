'use strict';

//Define the player component
angular.module('player').component('player', {
    templateUrl: 'app/player/player.template.html',
    require: {
        gameTable: '^gameTable'
    },
    bindings: {
        player: '=',
        index: '<'
    },
    controller: function PlayerController() {
        var self = this;

        //Increment the index so we don't start number the players from 0 which would be weird
        this.$onInit = function() {
            this.index = this.index+1;
        };

        //Player clicked the draw button, if they don't have active cards
        //call the drawCard method with the appropriate number of cards to draw
        //based on the game state
        this.playTurn = function() {
            if (self.player.getActiveCardValue() === false) {
                if (self.gameTable.state === 'playing') {
                    self.player.drawCard(1);
                } else if (self.gameTable.state === 'war') {
                    self.player.drawCard(2);
                }
                //Player action complete, trigger an update of the game state
                this.gameTable.evaluateCards();
            }
        };
    }
});