describe('Player', function() {
    var player;

    // Load the module that contains the 'Deck' service before each test
    beforeEach(module('warApp'));

    // Instantiate the service before each test
    beforeEach(inject(function(_playerFactory_) {
        factory = _playerFactory_;
        cards = [{ suit: 'clubs', value: 3, image: './app/deck/res/3c.png', imageBack: './app/deck/res/back.png' },
                 { suit: 'diamonds', value: 11, image: './app/deck/res/jd.png', imageBack: './app/deck/res/back.png' },
                 { suit: 'diamonds', value: 5, image: './app/deck/res/5d.png', imageBack: './app/deck/res/back.png' },
                 { suit: 'clubs', value: 13, image: './app/deck/res/kc.png', imageBack: './app/deck/res/back.png' },
                 { suit: 'clubs', value: 8, image: './app/deck/res/8c.png', imageBack: './app/deck/res/back.png' },
                 { suit: 'spades', value: 12, image: './app/deck/res/qs.png', imageBack: './app/deck/res/back.png' },
                 { suit: 'hearts', value: 11, image: './app/deck/res/jh.png', imageBack: './app/deck/res/back.png' }];
        player = factory.getNewPlayer(cards);
    }));

    it('should be alive', function() {
        var isAlive = player.isAlive();
        expect(isAlive).toBe(true);
    });

    it('should move one card from stack to active', function() {
        stackLength = player.stack.length;
        player.drawCard(1);
        expect(player.active.length).toEqual(1);
        expect(player.stack.length).toEqual(stackLength-1);
    });


    it('should return the value of the first active card', function() {
        player.active = [{ suit: 'clubs', value: 14, image: './app/deck/res/ac.png', imageBack: './app/deck/res/back.png' },
                         { suit: 'diamonds', value: 4, image: './app/deck/res/4d.png', imageBack: './app/deck/res/back.png' }];
        activeValue = player.getActiveCardValue();

        expect(activeValue).toEqual(14);
    });

    it('should move active cards into the hopper', function() {
        player.active = [{ suit: 'clubs', value: 14, image: './app/deck/res/ac.png', imageBack: './app/deck/res/back.png' },
                         { suit: 'diamonds', value: 4, image: './app/deck/res/4d.png', imageBack: './app/deck/res/back.png' }];

        player.moveActiveCardsToHopper();

        expect(player.active.length).toEqual(0);
        expect(player.hopper.length).toEqual(2);
    });

    it('should accept two cards and put them at the front of the stack', function() {
        cards = [{ suit: 'clubs', value: 14, image: './app/deck/res/ac.png', imageBack: './app/deck/res/back.png' },
                 { suit: 'diamonds', value: 4, image: './app/deck/res/4d.png', imageBack: './app/deck/res/back.png' }];
        player.acceptCards(cards);

        expect(player.stack[0].value).toEqual(4);
        expect(player.stack[0].suit).toEqual('diamonds');
        expect(player.stack[1].value).toEqual(14);
        expect(player.stack[1].suit).toEqual('clubs');
    });

    it('should empty active and hopper and return array of cards', function() {
        player.active = [{ suit: 'clubs', value: 14, image: './app/deck/res/ac.png', imageBack: './app/deck/res/back.png' },
                         { suit: 'diamonds', value: 4, image: './app/deck/res/4d.png', imageBack: './app/deck/res/back.png' }];
        player.hopper = [{ suit: 'hearts', value: 12, image: './app/deck/res/qc.png', imageBack: './app/deck/res/back.png' }];

        cards = player.forfeitCards();

        expect(player.active.length).toEqual(0);
        expect(player.hopper.length).toEqual(0);
        expect(cards.length).toEqual(3);
    });

    it('should switch alive status to false', function() {
        expect(player.stack.length).toEqual(7);
        player.verifyAliveStatus();
        expect(player.alive).toEqual(true);
        player.stack = [];
        player.verifyAliveStatus();
        expect(player.alive).toEqual(false);
    });

    it('should set player state to passed value', function() {
        player.setState('winner');
        expect(player.state).toBe('winner');
    });
});