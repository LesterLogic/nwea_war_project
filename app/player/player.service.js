'use strict';

//Define the playerFactory for creating new players
angular.module('player').
factory('playerFactory', function() {
    //The player prototype
    var Player = function(cards) {
        this.stack = cards;
        this.active = [];
        this.hopper = [];
        this.alive = true;
        this.state = 'default';

        //If there are cards to draw, draw the appropriate
        //number from the stack and add them to active
        this.drawCard = function(cardsToDraw) {
            this.verifyAliveStatus();
            if (this.isAlive()) {
                for (var x=0; x<cardsToDraw; x++) {
                    this.active.push(this.stack.pop());
                }
            }
        };

        //Check the active cards, use the value of the first (there could be more than one)
        this.getActiveCardValue = function() {
            return this.active.length > 0 ? this.active[0].value : false;
        };

        //Move the active cards to the hopper, these become spoils during a war
        this.moveActiveCardsToHopper = function() {
            this.hopper = this.hopper.concat(this.active);
            this.active = [];
        }

        //Receive cards after winning a battle
        //We pop cards off the stack, so we need to be add these to the beginning
        this.acceptCards = function(cards) {
            this.stack.reverse();
            this.stack = this.stack.concat(cards);
            this.stack.reverse();
        };

        //Collect all the cards from active and hopper and return them, clear out active and hopper
        this.forfeitCards = function() {
            var forfeits = [];
            forfeits = forfeits.concat(this.active, this.hopper);
            this.active = [];
            this.hopper = [];

            return forfeits;
        };

        //Check if there are still cards in the stack, if not the player is done playing
        this.verifyAliveStatus = function() {
            if (this.stack.length === 0) {
                this.alive = false;
            } else {
                this.alive = true;
            }
        }

        //Check if player is still allowed to play
        this.isAlive = function() {
            return this.alive;
        }

        //Set the player's state (default and winner are the only two states currently)
        this.setState = function(state) {
            this.state = state;
        }
    };

    return {
        //Create a new player with the array of cards passed in
        getNewPlayer: function(cards) {
            return new Player(cards);
        }
    };
});