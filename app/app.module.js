'use strict';

//Define the 'warApp' module
angular.module('warApp', [
    'ngRoute',
    'gameTable',
    'deck',
    'player'
]);