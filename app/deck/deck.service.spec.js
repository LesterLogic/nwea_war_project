describe('Deck', function() {
    var deck;

    // Load the module that contains the 'Deck' service before each test
    beforeEach(module('warApp'));

    // Instantiate the service before each test
    beforeEach(inject(function(_deckFactory_) {
        deck = _deckFactory_;
    }));

    it('should return a sorted deck of cards', function() {
        var cards = deck.getCards();

        expect(cards[0].value).toEqual(2);
        expect(cards[0].suit).toEqual('hearts');

        expect(cards[13].value).toEqual(2);
        expect(cards[13].suit).toEqual('diamonds');

        expect(cards[26].value).toEqual(2);
        expect(cards[26].suit).toEqual('clubs');

        expect(cards[39].value).toEqual(2);
        expect(cards[39].suit).toEqual('spades');
    });

    it('should return a shuffled deck of cards', function() {
        var sortedCards = deck.getCards();
        var shuffledCards = deck.shuffle(sortedCards);

        expect(shuffledCards).not.toEqual(sortedCards);
    });
});
