'use strict';

// Define the 'deck' module
// The deck module represents a classic deck of playing cards
angular.module('deck', []);