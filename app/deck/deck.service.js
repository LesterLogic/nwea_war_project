'use strict';

// Define the deckFactory for creating, retrieving, and shuffling the virtual deck
angular.module('deck').
factory('deckFactory', function() {
    //A classic deck of playing cards with aces high
    var cardValues = [
        {'suit':'hearts', 'value':2, 'image':'./app/deck/res/2h.png'},
        {'suit':'hearts', 'value':3, 'image':'./app/deck/res/3h.png'},
        {'suit':'hearts', 'value':4, 'image':'./app/deck/res/4h.png'},
        {'suit':'hearts', 'value':5, 'image':'./app/deck/res/5h.png'},
        {'suit':'hearts', 'value':6, 'image':'./app/deck/res/6h.png'},
        {'suit':'hearts', 'value':7, 'image':'./app/deck/res/7h.png'},
        {'suit':'hearts', 'value':8, 'image':'./app/deck/res/8h.png'},
        {'suit':'hearts', 'value':9, 'image':'./app/deck/res/9h.png'},
        {'suit':'hearts', 'value':10, 'image':'./app/deck/res/10h.png'},
        {'suit':'hearts', 'value':11, 'image':'./app/deck/res/jh.png'},
        {'suit':'hearts', 'value':12, 'image':'./app/deck/res/qh.png'},
        {'suit':'hearts', 'value':13, 'image':'./app/deck/res/kh.png'},
        {'suit':'hearts', 'value':14, 'image':'./app/deck/res/ah.png'},

        {'suit':'diamonds', 'value':2, 'image':'./app/deck/res/2d.png'},
        {'suit':'diamonds', 'value':3, 'image':'./app/deck/res/3d.png'},
        {'suit':'diamonds', 'value':4, 'image':'./app/deck/res/4d.png'},
        {'suit':'diamonds', 'value':5, 'image':'./app/deck/res/5d.png'},
        {'suit':'diamonds', 'value':6, 'image':'./app/deck/res/6d.png'},
        {'suit':'diamonds', 'value':7, 'image':'./app/deck/res/7d.png'},
        {'suit':'diamonds', 'value':8, 'image':'./app/deck/res/8d.png'},
        {'suit':'diamonds', 'value':9, 'image':'./app/deck/res/9d.png'},
        {'suit':'diamonds', 'value':10, 'image':'./app/deck/res/10d.png'},
        {'suit':'diamonds', 'value':11, 'image':'./app/deck/res/jd.png'},
        {'suit':'diamonds', 'value':12, 'image':'./app/deck/res/qd.png'},
        {'suit':'diamonds', 'value':13, 'image':'./app/deck/res/kd.png'},
        {'suit':'diamonds', 'value':14, 'image':'./app/deck/res/ad.png'},

        {'suit':'clubs', 'value':2, 'image':'./app/deck/res/2c.png'},
        {'suit':'clubs', 'value':3, 'image':'./app/deck/res/3c.png'},
        {'suit':'clubs', 'value':4, 'image':'./app/deck/res/4c.png'},
        {'suit':'clubs', 'value':5, 'image':'./app/deck/res/5c.png'},
        {'suit':'clubs', 'value':6, 'image':'./app/deck/res/6c.png'},
        {'suit':'clubs', 'value':7, 'image':'./app/deck/res/7c.png'},
        {'suit':'clubs', 'value':8, 'image':'./app/deck/res/8c.png'},
        {'suit':'clubs', 'value':9, 'image':'./app/deck/res/9c.png'},
        {'suit':'clubs', 'value':10, 'image':'./app/deck/res/10c.png'},
        {'suit':'clubs', 'value':11, 'image':'./app/deck/res/jc.png'},
        {'suit':'clubs', 'value':12, 'image':'./app/deck/res/qc.png'},
        {'suit':'clubs', 'value':13, 'image':'./app/deck/res/kc.png'},
        {'suit':'clubs', 'value':14, 'image':'./app/deck/res/ac.png'},

        {'suit':'spades', 'value':2, 'image':'./app/deck/res/2s.png'},
        {'suit':'spades', 'value':3, 'image':'./app/deck/res/3s.png'},
        {'suit':'spades', 'value':4, 'image':'./app/deck/res/4s.png'},
        {'suit':'spades', 'value':5, 'image':'./app/deck/res/5s.png'},
        {'suit':'spades', 'value':6, 'image':'./app/deck/res/6s.png'},
        {'suit':'spades', 'value':7, 'image':'./app/deck/res/7s.png'},
        {'suit':'spades', 'value':8, 'image':'./app/deck/res/8s.png'},
        {'suit':'spades', 'value':9, 'image':'./app/deck/res/9s.png'},
        {'suit':'spades', 'value':10, 'image':'./app/deck/res/10s.png'},
        {'suit':'spades', 'value':11, 'image':'./app/deck/res/js.png'},
        {'suit':'spades', 'value':12, 'image':'./app/deck/res/qs.png'},
        {'suit':'spades', 'value':13, 'image':'./app/deck/res/ks.png'},
        {'suit':'spades', 'value':14, 'image':'./app/deck/res/as.png'},
    ];

    //The Total War deck for testing
    /*
    var cardValues = [
        {'suit':'hearts', 'value':7, 'image':'./app/deck/res/7h.png'},
        {'suit':'hearts', 'value':7, 'image':'./app/deck/res/7h.png'},
        {'suit':'hearts', 'value':7, 'image':'./app/deck/res/7h.png'},
        {'suit':'hearts', 'value':7, 'image':'./app/deck/res/7h.png'},
        {'suit':'hearts', 'value':7, 'image':'./app/deck/res/7h.png'},
        {'suit':'hearts', 'value':7, 'image':'./app/deck/res/7h.png'},
        {'suit':'hearts', 'value':7, 'image':'./app/deck/res/7h.png'},
        {'suit':'hearts', 'value':7, 'image':'./app/deck/res/7h.png'},
        {'suit':'hearts', 'value':7, 'image':'./app/deck/res/7h.png'},
        {'suit':'hearts', 'value':7, 'image':'./app/deck/res/7h.png'},
        {'suit':'hearts', 'value':7, 'image':'./app/deck/res/7h.png'},
        {'suit':'hearts', 'value':7, 'image':'./app/deck/res/7h.png'},
        {'suit':'hearts', 'value':7, 'image':'./app/deck/res/7h.png'},

        {'suit':'diamonds', 'value':7, 'image':'./app/deck/res/7d.png'},
        {'suit':'diamonds', 'value':7, 'image':'./app/deck/res/7d.png'},
        {'suit':'diamonds', 'value':7, 'image':'./app/deck/res/7d.png'},
        {'suit':'diamonds', 'value':7, 'image':'./app/deck/res/7d.png'},
        {'suit':'diamonds', 'value':7, 'image':'./app/deck/res/7d.png'},
        {'suit':'diamonds', 'value':7, 'image':'./app/deck/res/7d.png'},
        {'suit':'diamonds', 'value':7, 'image':'./app/deck/res/7d.png'},
        {'suit':'diamonds', 'value':7, 'image':'./app/deck/res/7d.png'},
        {'suit':'diamonds', 'value':7, 'image':'./app/deck/res/7d.png'},
        {'suit':'diamonds', 'value':7, 'image':'./app/deck/res/7d.png'},
        {'suit':'diamonds', 'value':7, 'image':'./app/deck/res/7d.png'},
        {'suit':'diamonds', 'value':7, 'image':'./app/deck/res/7d.png'},
        {'suit':'diamonds', 'value':7, 'image':'./app/deck/res/7d.png'},

        {'suit':'clubs', 'value':7, 'image':'./app/deck/res/7c.png'},
        {'suit':'clubs', 'value':7, 'image':'./app/deck/res/7c.png'},
        {'suit':'clubs', 'value':7, 'image':'./app/deck/res/7c.png'},
        {'suit':'clubs', 'value':7, 'image':'./app/deck/res/7c.png'},
        {'suit':'clubs', 'value':7, 'image':'./app/deck/res/7c.png'},
        {'suit':'clubs', 'value':7, 'image':'./app/deck/res/7c.png'},
        {'suit':'clubs', 'value':7, 'image':'./app/deck/res/7c.png'},
        {'suit':'clubs', 'value':7, 'image':'./app/deck/res/7c.png'},
        {'suit':'clubs', 'value':7, 'image':'./app/deck/res/7c.png'},
        {'suit':'clubs', 'value':7, 'image':'./app/deck/res/7c.png'},
        {'suit':'clubs', 'value':7, 'image':'./app/deck/res/7c.png'},
        {'suit':'clubs', 'value':7, 'image':'./app/deck/res/7c.png'},
        {'suit':'clubs', 'value':7, 'image':'./app/deck/res/7c.png'},

        {'suit':'spades', 'value':7, 'image':'./app/deck/res/7s.png'},
        {'suit':'spades', 'value':7, 'image':'./app/deck/res/7s.png'},
        {'suit':'spades', 'value':7, 'image':'./app/deck/res/7s.png'},
        {'suit':'spades', 'value':7, 'image':'./app/deck/res/7s.png'},
        {'suit':'spades', 'value':7, 'image':'./app/deck/res/7s.png'},
        {'suit':'spades', 'value':7, 'image':'./app/deck/res/7s.png'},
        {'suit':'spades', 'value':7, 'image':'./app/deck/res/7s.png'},
        {'suit':'spades', 'value':7, 'image':'./app/deck/res/7s.png'},
        {'suit':'spades', 'value':7, 'image':'./app/deck/res/7s.png'},
        {'suit':'spades', 'value':7, 'image':'./app/deck/res/7s.png'},
        {'suit':'spades', 'value':7, 'image':'./app/deck/res/7s.png'},
        {'suit':'spades', 'value':7, 'image':'./app/deck/res/7s.png'},
        {'suit':'spades', 'value':7, 'image':'./app/deck/res/7s.png'},
    ];
    */

    //The card object prototype
    var Card = function(cardValues) {
        this.suit = cardValues['suit'];
        this.value = cardValues['value'];
        this.image = cardValues['image'];
        this.imageBack = './app/deck/res/back.png';
    };

    //The deck prototype
    var Deck = function() {
        //Iterate over the card values and create new cards for the deck
        this.cards = [];
        for (var x=0; x<cardValues.length; x++) {
            this.cards.push(new Card(cardValues[x]));
        }

        //Return a shuffled copy of the deck
        //Doesn't act on the card property directly so we can use
        //the shuffle method on smaller batches of cards later on
        this.shuffle = function(cards) {
            var shuffledCards = [];
            var j, x, i;
            for (var i=0; i<cards.length; i++) {
                j = Math.floor(Math.random() * shuffledCards.length);
                x = cards[i];
                shuffledCards.splice(j, 0, x);
            }

            return shuffledCards;
        }

        //Return the sorted list of cards
        this.getCards = function() {
            return this.cards;
        }
    }

    return new Deck();
});