'use strict';

//Configuration for the warApp module
//If the app needs more views or routing this is the place to add them
angular.module('warApp').config(['$routeProvider',
    function config($routeProvider) {
        $routeProvider.
            when('/', {
                template: '<game-table></game-table>'
            }).
            otherwise('/');
    }
]);