# README #

Installation:

1. Clone repo
2. cd <application directory>
3. npm install

Dependencies:
PhantomJS
Jasmine

The application is set up to use PhantomJS in Karma as the testing browser. If you have another browser available on your system (e.g. Chrome), you can swap out the PhantomJS requirements in karma.conf.js

1. npm uninstall karma-phantomjs-launcher
2. npm install --save-dev karma-chrome-launcher
3. Edit karma.conf.js and replace PhantomJS with Chrome on line 19
4. Edit karma.conf.js and replace karma-phantomjs-launcher with karma-chrome-launcher
5. Save karma.conf.js

Testing:
To run tests
1. cd <application directory>
2. npm run test

This will launch Karma, run all tests, and begin watching for source changes